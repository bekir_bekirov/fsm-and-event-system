/*
 * EventManager.c
 *
 *  Created on: 4 авг. 2020 г.
 *      Author: bekir
 */
#include "EventManager.h"
#include <stdatomic.h>

void Func_1(Event_t* event)
{
	printf("Event is %u\n", event->id);
}

void Func_2(Event_t* event)
{
	printf("Event is %u\n", event->id);
}

void Func_3(Event_t* event)
{
	printf("Event is %u\n", event->id);
}

static const EventDatabase_t event_db[] =
{
#include "event_manager_table.h"
};

static LIST_HEAD(list_item_head);

EventManager_t event_manager = {event_db, &list_item_head};

void SendEvent(EventManager_t* evn_mng, Event_t* event)
{
	/* Initialize each node's list entry */
	if (event)
	{
		INIT_LIST_HEAD(&event->list_item);
		event->state = EV_ACTIVE;
		list_add_tail(&event->list_item, evn_mng->head_list_item);
	}
}

static void DispatchEvent(EventManager_t* evn_mng, Event_t* event)
{
//	uint32_t event_cnt = sizeof(event_mng) / sizeof(EventDatabase_t);

	uint32_t i = 0;

	while(evn_mng->database[i].id != DB_EVENT_LAST_ITEM)
	{
		if ((event) && (event->state == EV_ACTIVE))
		{
			if (event->id == evn_mng->database[i].id)
			{
				if (evn_mng->database[i].handle_func_ptr)
				{
					evn_mng->database[i].handle_func_ptr(event);
					event->state = EV_NO_ACTIVE;
					event->param = NULL;
					break;
				}
			}
		}
		i++;
	}
}

void DispatchProcessEvent(EventManager_t* evn_mng, Event_t* event)
{
	DispatchEvent(evn_mng, event);
	if ((event) && (event->state == EV_ACTIVE)) list_del(&event->list_item);
}

void ProcessEvents(EventManager_t* evn_mng)
{
	Event_t* it;
	Event_t* next;

	list_for_each_entry_safe(it, next, evn_mng->head_list_item, list_item){
		DispatchEvent(evn_mng, it);
		list_del(&it->list_item);
	}
}

EventManager_t* GetPtrToEventManager()
{
	return &event_manager;
}



