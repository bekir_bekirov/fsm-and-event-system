/*
 * FSM.h
 *
 *  Created on: 14 авг. 2020 г.
 *      Author: bekir
 */

#ifndef FSM_H_
#define FSM_H_

#include <stdio.h>
#include <stdint.h>
#include <stdatomic.h>
#include "EventManager.h"

typedef uint32_t FSMEvent_t;

typedef struct TestStruct
{
	void* param;
	uint32_t next_state;
}TestStruct_t;

struct FSM_tr;
typedef void (*FSMHandle)(FSMEvent_t* ptr);

typedef struct FSM_tr
{
	uint32_t state;
	uint32_t next_state;
	FSMEvent_t* event;
	FSMHandle enter_ptr_handle;
	FSMHandle action_ptr_handle;
	FSMHandle exit_ptr_handle;
	FSMHandle guard_ptr_handle;
}FSM_tr_t;

typedef struct FSM
{
	const FSM_tr_t* tr_table_ptr;
}FSM_t;

void FSMProcess(const FSM_t* fsm);
const FSM_t* GetFSMObj(void);

#endif /* FSM_H_ */
