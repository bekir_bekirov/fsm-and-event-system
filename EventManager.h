/*
 * EventManager.h
 *
 *  Created on: 4 авг. 2020 г.
 *      Author: bekir
 */

#ifndef EVENTMANAGER_H_
#define EVENTMANAGER_H_

#include "list.h"
#include "stdint.h"
#include "stdio.h"

#define DB_EVENT_LAST_ITEM	0xFFFFFFFF

typedef enum{EV_NO_ACTIVE = 0, EV_ACTIVE} EVENT_STATE;

typedef struct Event
{
	uint32_t id;
	EVENT_STATE state;
	void* param;
	struct list_head list_item;
}Event_t;

typedef void(*HandlerFuncPtr)(Event_t* event);

typedef struct EventDatabase
{
	uint32_t id;
	HandlerFuncPtr handle_func_ptr;
}EventDatabase_t;

typedef struct EventManager
{
	const EventDatabase_t* database;
	struct list_head* head_list_item;
}EventManager_t;

void SendEvent(EventManager_t* evn_mng, Event_t* event);
void ProcessEvents(EventManager_t* evn_mng);
void DispatchProcessEvent(EventManager_t* evn_mng, Event_t* event);
EventManager_t* GetPtrToEventManager(void);

#endif /* EVENTMANAGER_H_ */
