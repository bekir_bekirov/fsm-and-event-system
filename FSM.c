/*
 * FSM.c
 *
 *  Created on: 14 авг. 2020 г.
 *      Author: bekir
 */

#include "EventManager.h"
#include "FSM.h"
#include <unistd.h>

TestStruct_t test = {NULL, 0};

FSMEvent_t fsm_event_1;
FSMEvent_t fsm_event_2;

Event_t event_fsm_1 = {
					.id = 1,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

Event_t event_fsm_2 = {
					.id = 2,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

Event_t event_fsm_3 = {
					.id = 3,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

void FSMEvent1_cb(Event_t* event)
{
	printf("Event is %u\n", event->id);
	fsm_event_1 = 1;
	event->param = &fsm_event_1;
}

void FSMEvent2_cb(Event_t* event)
{
	printf("Event is %u\n", event->id);
	fsm_event_2 = 1;
	event->param = &fsm_event_2;
}

static const EventDatabase_t event_db[] =
{
		{1, &FSMEvent1_cb},
		{2, &FSMEvent2_cb},
		{DB_EVENT_LAST_ITEM, NULL},
};

static LIST_HEAD(list_item_head_event_fsm);

EventManager_t event_manager_fsm = {event_db, &list_item_head_event_fsm};



void action_1(FSMEvent_t* ptr)
{
	printf("State 1\n");
	SendEvent(&event_manager_fsm, &event_fsm_1);
}

void action_2(FSMEvent_t* ptr)
{
	if (*ptr == 1)
	{
	printf("State 2\n");
	SendEvent(&event_manager_fsm, &event_fsm_2);
	}
}

void action_3(FSMEvent_t* ptr)
{
	printf("State 3\n");
	//SendEvent(&event_manager_fsm, &event_fsm_2);
}

static const FSM_tr_t fsm_tr[] =
{
		{0,				1,				NULL,				NULL,				&action_1,				NULL,				NULL},
		{1,				0,				&fsm_event_1,		NULL, 				&action_2, 				NULL, 				NULL},
		{2,				0,				&fsm_event_2,		NULL,				&action_3,				NULL,				NULL},
		{0xFFFFFFFF,		0xFFFFFFFF,				NULL,				NULL,				NULL,				NULL,				NULL},
};

FSM_t fsm = {fsm_tr};

const FSM_t* GetFSMObj(void)
{
	return &fsm;
}

void FSMProcess(const FSM_t* fsm)
{
	static uint32_t indx = 0;
	uint32_t i = 0;

	ProcessEvents(&event_manager_fsm);

	while (fsm->tr_table_ptr[i].state != 0xFFFFFFFF)
	{

		if (fsm->tr_table_ptr[i].state == indx)
		{
			if (fsm->tr_table_ptr[i].event)
			{
				if (*fsm->tr_table_ptr[i].event == 1)
				{
					if (fsm->tr_table_ptr[i].action_ptr_handle) fsm->tr_table_ptr[i].action_ptr_handle(fsm->tr_table_ptr[i].event);
					*fsm->tr_table_ptr[i].event = 0;
					indx = fsm->tr_table_ptr[i].next_state;
				}
				return;
			}
		}
		++i;
	}
	if (fsm->tr_table_ptr[indx].action_ptr_handle) fsm->tr_table_ptr[indx].action_ptr_handle(fsm->tr_table_ptr[indx].event);
	indx = fsm->tr_table_ptr[indx].next_state;
	//DispatchProcessEvent(&event_manager_fsm, fsm->tr_table_ptr[indx].event);

	usleep(100000);
}

