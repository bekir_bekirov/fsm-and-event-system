/*
 * main.c
 *
 *  Created on: 4 авг. 2020 г.
 *      Author: bekir
 */

#include <stdio.h>
#include <stdint.h>
#include <stdatomic.h>
#include "EventManager.h"
#include "FSM.h"

Event_t event_1 = {
					.id = 1,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

Event_t event_2 = {
					.id = 2,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

Event_t event_3 = {
					.id = 3,
					.state = 0,
					.param = NULL,
					.list_item = {NULL, NULL}
					};

int main (int argc, char* argv[])
{
	printf("Hello World!\n");

	SendEvent(GetPtrToEventManager(), &event_1);
	SendEvent(GetPtrToEventManager(), &event_2);
	SendEvent(GetPtrToEventManager(), &event_3);

	ProcessEvents(GetPtrToEventManager());

	SendEvent(GetPtrToEventManager(), &event_1);
	SendEvent(GetPtrToEventManager(), &event_2);

	ProcessEvents(GetPtrToEventManager());

	while(1) FSMProcess(GetFSMObj());

	return 0;
}
